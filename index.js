"use strict";

const usersURL = "https://ajax.test-danit.com/api/json/users";
const postsURL = "https://ajax.test-danit.com/api/json/posts";

class Required {
  get(url) {
    return fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => response.json());
  }
  delete(url) {
    return fetch(url, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => response);
  }
  post(url, postObj) {
    return fetch(url, {
      method: "POST",
      body: JSON.stringify(postObj),
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => response.json());
  }
  put(url, postObj) {
    return fetch(url, {
      method: "PUT",
      body: JSON.stringify(postObj),
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => response.json());
  }
}

class Card {
  render(post, author, postUrl = "") {
    const card = document.createElement("div");
    card.classList.add("card");
    const changePost = document.createElement("button");
    changePost.classList.add("card-change");
    changePost.innerHTML =
      '<svg fill="#000000" height="15px" width="15px" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 306.637 306.637" xml:space="preserve"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <g> <g> <path d="M12.809,238.52L0,306.637l68.118-12.809l184.277-184.277l-55.309-55.309L12.809,238.52z M60.79,279.943l-41.992,7.896 l7.896-41.992L197.086,75.455l34.096,34.096L60.79,279.943z"></path> <path d="M251.329,0l-41.507,41.507l55.308,55.308l41.507-41.507L251.329,0z M231.035,41.507l20.294-20.294l34.095,34.095 L265.13,75.602L231.035,41.507z"></path> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </g> </g></svg>';

    changePost.addEventListener("click", (e) => {
      e.preventDefault();
      const card = e.target.closest(".card");
      const modal = new Modal();
      document.body.append(
        modal.render(
          author.name,
          author.email,
          post.title,
          post.body,
          modal.changePostHandler,
          postUrl,
          document.body.main,
          post,
          card
        )
      );
    });

    const deletePost = document.createElement("button");
    deletePost.classList.add("card-delete");
    deletePost.innerHTML =
      '<svg width="15" height="15" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">   <path d="M2.72298 0.126929C2.68937 0.207043 2.67817 0.377283 2.70058 0.507467C2.73419 0.737793 2.7566 0.737793 4.02263 0.737793H5.31107V0.387297V0.0368019L4.04504 0.00675946C3.02549 -0.0132689 2.779 0.00675946 2.72298 0.126929Z" fill="#151540"/>          <path d="M0.0340655 1.32863C0.000453961 1.40874 -0.0107499 1.57898 0.0116578 1.70917C0.0340655 1.88942 0.123696 1.94951 0.448608 1.96954L0.851946 1.99958L0.930373 3.22131C0.975189 3.89225 1.07602 5.16405 1.15445 6.03528C1.34492 8.13826 1.13204 7.99806 3.98902 7.99806C5.18783 7.99806 6.2522 7.958 6.36424 7.89792C6.72276 7.72768 6.80119 7.29707 7.00286 4.62329L7.19332 1.99958L7.59666 1.96954C7.97759 1.93949 8 1.91947 8 1.589V1.2385L4.04504 1.20846C0.695092 1.18843 0.0900847 1.20846 0.0340655 1.32863ZM6.01692 4.31285C5.91608 5.59466 5.82645 6.76632 5.82645 6.91653L5.81525 7.19693H4.02263H2.23002V6.91653C2.23002 6.76632 2.15159 5.81497 2.06196 4.79353C1.97233 3.77208 1.8939 2.73061 1.8939 2.46023V1.98956H4.05625H6.20738L6.01692 4.31285Z" fill="#151540"/></svg>';

    deletePost.addEventListener("click", () => {
      if (post.id > 100) {
        card.remove();
        return;
      }
      const required = new Required();
      required
        .delete(postUrl + `/${post.id}`)
        .then((response) => {
          if (response.ok === true && response.status === 200) {
            card.remove();
          }
        })
        .catch((error) => {
          console.error(error.message);
        });
    });

    const cardAuthor = document.createElement("h2");
    cardAuthor.classList.add("card-author");

    const cardAuthorEmail = document.createElement("p");
    cardAuthorEmail.classList.add("card-email");

    const cardTitle = document.createElement("p");
    cardTitle.classList.add("card-title");

    const cardText = document.createElement("p");
    cardText.classList.add("card-text");

    cardAuthor.textContent = author.name;
    cardAuthorEmail.textContent = author.email;
    cardTitle.textContent = post.title;
    cardText.textContent = post.body;

    card.append(
      changePost,
      deletePost,
      cardAuthor,
      cardAuthorEmail,
      cardTitle,
      cardText
    );

    return card;
  }
}

class Post {
  getPosts(url) {
    const required = new Required();
    return required.get(url);
  }
  getUsers(url) {
    const required = new Required();
    return required.get(url);
  }
  createPosts(postUrl, usersUrl) {
    const animation = document.querySelector(".holder");

    const postElements = document.createElement("div");

    this.getPosts(postUrl)
      .then((posts) => {
        this.getUsers(usersUrl)
          .then((users) => {
            animation.style.display = "none";

            posts.forEach((post) => {
              const author = users.find((user) => user.id === post.userId);
              const card = new Card();
              postElements.append(card.render(post, author, postUrl));
            });
          })
          .catch((error) => {
            console.error(error.message);
          });
      })
      .catch((error) => {
        console.error(error.message);
      });
    return postElements;
  }
}

class Modal {
  render(
    authorInput = "",
    emailInput = "",
    titleInput = "",
    contentInput = "",
    handler,
    postUrl = "",
    root = document.body.main,
    post,
    card
  ) {
    const modal = document.createElement("div");
    modal.classList.add("modal");

    const form = document.createElement("form");
    form.classList.add("modal-form");
    const author = document.createElement("input");
    author.placeholder = "Author name";
    author.value = authorInput;
    const email = document.createElement("input");
    email.value = emailInput;
    email.placeholder = "Author email";
    const postTitle = document.createElement("input");
    postTitle.value = titleInput;
    postTitle.placeholder = "Post title";
    const postText = document.createElement("textarea");
    postText.value = contentInput;
    postText.placeholder = "Post content";
    const postCreate = document.createElement("button");
    if (contentInput === "") {
      postCreate.textContent = "Create post";
    } else {
      postCreate.textContent = "Change post";
    }
    postCreate.addEventListener("click", (e) => {
      e.preventDefault();
      handler(author, email, postTitle, postText, postUrl, root, post, card);
      modal.remove();
    });
    form.append(author, email, postTitle, postText, postCreate);
    modal.append(form);
    return modal;
  }
  createPostHandler(author, email, title, content, url, root, post, card) {
    const required = new Required();
    required
      .post(url, { body: content.value, title: title.value, userId: 1 })
      .then((response) => {
        const card = new Card();
        root.prepend(
          card.render(response, { name: author.value, email: email.value }, url)
        );
      })
      .catch((error) => {
        console.error(error.message);
      });
  }
  changePostHandler(author, email, title, content, url, root, post, card) {
    const required = new Required();
    required
      .put(`${url}/${post.id}`, {
        body: content.value,
        title: title.value,
        userId: 1,
      })
      .then((response) => {
        card.querySelector(".card-author").textContent = author.value;
        card.querySelector(".card-email").textContent = email.value;
        card.querySelector(".card-title").textContent = title.value;
        card.querySelector(".card-text").textContent = content.value;
      })
      .catch((error) => {
        if (post.id > 100) {
          card.querySelector(".card-author").textContent = author.value;
          card.querySelector(".card-email").textContent = email.value;
          card.querySelector(".card-title").textContent = title.value;
          card.querySelector(".card-text").textContent = content.value;
        }else{
          console.error(error.message);
        }
      });
  }
}

const posts = new Post();
const root = document.querySelector("#root");

const createNewPost = document.createElement("button");
createNewPost.textContent = "Create post";
createNewPost.addEventListener("click", () => {
  const modal = new Modal();
  document.body.append(
    modal.render("", "", "", "", modal.createPostHandler, postsURL, root)
  );
});

document.body.prepend(createNewPost);
root.append(posts.createPosts(postsURL, usersURL));
